extern crate reqwest;
extern crate serde;
#[macro_use]
extern crate serde_derive;
extern crate serde_yaml;
extern crate lettre;

use lettre::email::EmailBuilder;
use lettre::transport::smtp::{SecurityLevel, SmtpTransportBuilder};
use lettre::transport::smtp::authentication::Mechanism;
use lettre::transport::EmailTransport;
use std::collections::HashMap;
use std::sync::mpsc::{channel, Sender, Receiver};
use std::fs::File;
use std::io::BufReader;
use std::ops::Deref;
use std::thread;
use std::thread::sleep;
use std::time::{Duration, Instant};



#[derive(Debug, PartialEq, Serialize, Deserialize, Clone)]
enum HostType {
    #[serde(rename = "http")]
    HTTP,
    #[serde(rename = "ping")]
    PING,
}

#[derive(Debug, PartialEq, Serialize, Deserialize, Clone)]
struct EmailConfig {
    host: String,
    port: u16,
    to: String,
    from: String,
    user: String,
    pass: String,
}

#[derive(Debug, Hash, PartialEq, Eq, Serialize, Deserialize, Clone)]
struct HostId(String);

impl Deref for HostId {
    type Target = String;

    fn deref(&self) -> &String {
        &self.0
    }
}

#[derive(Debug, PartialEq, Serialize, Deserialize, Clone)]
struct Host {
    id: HostId,
    uri: String,
    interval: u32,
    #[serde(rename = "type")]
    host_type: HostType,
    #[serde(default)]
    down: bool,
}

#[derive(Debug, PartialEq, Serialize, Deserialize)]
struct Config {
    hosts: Vec<Host>,
    email: EmailConfig,
}

#[derive(Debug)]
struct PingResult {
    duration: Duration,
    host_id: HostId,
    status: u16,
}


fn worker(channel: Sender<Result<PingResult, PingResult>>, host: Host) {
    loop {
        println!("Fetching {}...", &host.uri);

        let start = Instant::now();
        let resp_result = reqwest::get(&host.uri);
        let status = resp_result.as_ref().map(|r|{r.status()}).unwrap_or(reqwest::StatusCode::Unregistered(0));
        let duration = start.elapsed();

        let result = PingResult {
            host_id: host.id.clone(),
            duration: duration,
            status: status.into(),
        };

        if resp_result.is_ok() && status.is_success() {
            channel
                .send(Ok(result))
                .expect("Could not send value on channel");
        } else {
            channel
                .send(Err(result))
                .expect("Could not send error on channel.");
        }

        sleep(Duration::new(host.interval as u64, 0) - duration);
    }
}


fn spawn_workers(config: &Config) -> Receiver<Result<PingResult, PingResult>> {
    let (sender, receiver) = channel();
    for host in &config.hosts {
        let chost = host.clone();
        let csender = sender.clone();
        thread::spawn(move || { worker(csender, chost); });
    }

    // Return the receiver so we can receive the messages that channels send.
    receiver
}


fn notify(state: bool, host_id: &HostId, config: &Config) {
    let host = config.hosts.iter().find(|host| host.id == *host_id).expect("Host not found in the config.");
    let subject: String;
    let body: String;

    if state {
        subject = format!("Panoptes: {} is back up.", host_id.0);
        body = format!("The host {} is back up.", &host.uri);
        println!("Phew, {} is back up.", host.uri);
    } else {
        subject = format!("Panoptes: {} is down.", host_id.0);
        body = format!("The host {} is down.", &host.uri);
        println!("OMGOMGOMG, {} is down!", host.uri);
    }

    let email = EmailBuilder::new()
        .to(config.email.to.as_str())
        .from(config.email.from.as_str())
        .subject(subject.as_str())
        .text(body.as_str())
        .build().unwrap();

    let mut mailer = SmtpTransportBuilder::new((config.email.host.as_str(), config.email.port)).unwrap()
        // Add credentials for authentication
        .credentials(&config.email.user, &config.email.pass)
        .security_level(SecurityLevel::AlwaysEncrypt)
        .smtp_utf8(true)
        .authentication_mechanism(Mechanism::Plain)
        .build();

    let result = mailer.send(email);
    if result.is_err() {
        println!("Could not send email!: {}", result.unwrap_err());
    }
}

fn act_on_result(config: &Config, host_state: &mut HashMap<HostId, bool>, result: Result<PingResult, PingResult>) {
    let ok = result.is_ok();
    let r = result.unwrap_or_else(|r| r);
    let previous_ok = *host_state.entry(r.host_id.clone()).or_insert(true);

    println!(
        "Request to {} {} in {} secs.",
        &*r.host_id,
        if ok { "succeeded" } else { "failed" },
        r.duration.as_secs() as f64 + r.duration.subsec_nanos() as f64 * 1e-9
    );

    if previous_ok != ok {
        notify(ok, &r.host_id, config);
    }

    &host_state.insert(r.host_id.clone(), ok);
}


fn main() {
    println!("Starting Panoptes...");

    let config_file = File::open("config.yaml").expect("Could not open config.yaml.");
    let reader = BufReader::new(config_file);
    let config: Config = serde_yaml::from_reader(reader).expect("Could not parse config file.");

    let mut host_state: HashMap<HostId, bool> = HashMap::new();
    let receiver = spawn_workers(&config);

    loop {
        act_on_result(
            &config,
            &mut host_state,
            receiver
                .recv()
                .expect("Could not receive result from channel."),
        );
    }
}
